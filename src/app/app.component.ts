import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Firebase } from '@ionic-native/firebase';
import { NativeStorage } from '@ionic-native/native-storage';
import { TranslateService , LangChangeEvent} from '@ngx-translate/core';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: string = "ProfileSettingPage";

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public firebase: Firebase, private nativeStorage: NativeStorage, public translate: TranslateService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

        // ENGLISH
      translate.setDefaultLang('en');
      translate.use('en');

      translate.onLangChange.subscribe((event:LangChangeEvent) => {
       console.log('onLangChange', event.translations);
      });

      console.log(platform);
      this.nativeStorage.getItem('skip')
      .then(
        data => {
          console.log(data)
          if(data)            
            this.rootPage = "PreloginPage";
        },
        error => console.error(error)
      );

      if (platform.is('cordova')) {
        firebase.getToken()
          .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
          .catch(error => console.error('Error getting token', error));

        firebase.onTokenRefresh()
          .subscribe((token: string) => console.log(`Got a new token ${token}`));


        firebase.hasPermission()
          .then(data => {
            console.log(`The permission is ${data}`)
            if (!data.isEnabled) {
              this.grantPermission()
            }
          }).catch(error => console.error('Error getting permission', error));
      }

    });
  }

  grantPermission() {
    this.firebase.grantPermission()
      .then(resp => console.log(`The token is ${resp}`)) // save the token server-side and use it to push notifications to this device
      .catch(error => console.error('Error granting permission', error));
  }

}
