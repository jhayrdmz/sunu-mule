import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExplanationPage } from './explanation';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ExplanationPage,
  ],
  imports: [
    IonicPageModule.forChild(ExplanationPage),
    TranslateModule.forChild()
  ],
})
export class ExplanationPageModule {}
