import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationChildPage } from './registration-child';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RegistrationChildPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationChildPage),
    TranslateModule.forChild()
  ],
})
export class RegistrationChildPageModule {}
