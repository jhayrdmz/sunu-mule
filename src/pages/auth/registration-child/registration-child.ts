import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage';
import { ApiProvider } from './../../../providers/api/api';

/**
 * Generated class for the RegistrationChildPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registration-child',
  templateUrl: 'registration-child.html',
})
export class RegistrationChildPage {
  
  public registerChildForm : FormGroup;
  public child_type: string;
  public child_name: string;  
  public child_birthday:any;
  public user_id: any;
  public email: any;
  public getLocal: any;
  public regData:{};
  public responseData;

 

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private nativeStorage: NativeStorage,
    public api: ApiProvider,
    public alertCtrl: AlertController,) {

    this.child_type = 'daughter';
    
    this.registerChildForm = this.formBuilder.group({
      name: ['', Validators.required],
      date: ['', Validators.required]          
      // password: ['', Validators.required,Validators.minLength(5)]
    });

    api.getLocalStorage('parent_data').subscribe((data) => {
        console.log(data);
        this.email = data.email
    });

    api.getLocalStorage('signup_parent').subscribe((data) => {
        console.log(data);
        this.user_id = data.user_id
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationChildPage');
  }
  tabSelect(tabType){
    this.child_type = tabType;
  }
  showAlert(title,message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  // gotoPage(page, pageType?) {
  //   if (!!page) {
  //     if(!!pageType) {
  //       if(pageType.toLowerCase() === 'root') {
  //         this.navCtrl.setRoot(page,  {}, {animate: false, direction: 'back'});
  //       }
  //     } else {
  //       this.navCtrl.push(page,{},{animate:false});
  //     }
  //   }
  //   page = null;
  // }
  

  registerChild(page, pageType?){
    
    this.regData = {   
      'child_birthday': this.child_birthday, 
      'child_name': this.child_name,
      'child_type': this.child_type,
      'email': this.email,
      'user_id': this.user_id
    }
    console.log(this.regData)
    this.api.childRegister(this.regData).then(
      data => {      
        this.responseData = data;
        console.log(this.responseData);
        if(this.responseData.status == "OK"){       
          this.nativeStorage.setItem('child_data', {child_name:this.child_name,child_birthday: this.child_birthday,child_type: this.child_type,parent_id:this.user_id});
          this.nativeStorage.setItem('signup_child', this.responseData.result);
          this.navCtrl.push(page,{},{animate:false});
        }     
      
    },
      err => {
        console.log(err);
        console.log(err.status);
        if(err.status == 400){
          console.log(err.error.result.message)
          this.showAlert('Oops!',err.error.result.message);
        } else{
          this.showAlert('Oops!','Something went wrong.');
        }        
      }
    );
  }
  

}
