import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationPhotoPage } from './registration-photo';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RegistrationPhotoPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationPhotoPage),
    TranslateModule.forChild()
  ],
})
export class RegistrationPhotoPageModule {}
