import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PreloginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prelogin',
  templateUrl: 'prelogin.html',
})
export class PreloginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreloginPage');
  }

 	gotoPage(page, pageType?) {
		if (!!page) {
			if(!!pageType) {
				if(pageType.toLowerCase() === 'root') {
					this.navCtrl.setRoot(page,  {}, {animate: false, direction: 'back'});
				}
			} else {
				this.navCtrl.push(page,{},{animate:false});
			}
		}
		page = null;
 	}

}
