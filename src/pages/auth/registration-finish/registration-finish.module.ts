import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationFinishPage } from './registration-finish';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    RegistrationFinishPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistrationFinishPage),
    TranslateModule.forChild()
  ],
})
export class RegistrationFinishPageModule {}
