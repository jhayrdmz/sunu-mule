import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TourFinishPage } from './tour-finish';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    TourFinishPage,
  ],
  imports: [
    IonicPageModule.forChild(TourFinishPage),
    TranslateModule.forChild()
  ],
})
export class TourFinishPageModule {}
