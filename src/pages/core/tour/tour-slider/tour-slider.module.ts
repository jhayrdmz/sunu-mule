import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TourSliderPage } from './tour-slider';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    TourSliderPage,
  ],
  imports: [
    IonicPageModule.forChild(TourSliderPage),
    TranslateModule.forChild()
  ],
})
export class TourSliderPageModule {}
