import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from './../../../../providers/api/api';

/**
 * Generated class for the TourStartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tour-start',
  templateUrl: 'tour-start.html',
})
export class TourStartPage {

  public child_name: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public api: ApiProvider,
    ) {
    api.getLocalStorage('child_data').subscribe((data) => {
        console.log(data);
        this.child_name = data.child_name
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TourStartPage');
  }

  gotoPage(page, pageType?) {
    if (!!page) {
      if(!!pageType) {
        if(pageType.toLowerCase() === 'root') {
          this.navCtrl.setRoot(page,  {}, {animate: false, direction: 'back'});
        }
      } else {
        this.navCtrl.push(page,{},{animate:false});
      }
    }
    page = null;
  }

}
