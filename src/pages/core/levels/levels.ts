import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LevelsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-levels',
  templateUrl: 'levels.html',
})
export class LevelsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log('load tes');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LevelsPage');
  }

  gotoPage(page, pageType?) {
    console.log('click test');
    if (!!page) {
      if(!!pageType) {
        if(pageType.toLowerCase() === 'root') {
          this.navCtrl.setRoot(page,  {}, {animate: false, direction: 'back'});
        }
      } else {
        this.navCtrl.push(page,{},{animate:false});
      }
    }
    page = null;
  }
}
