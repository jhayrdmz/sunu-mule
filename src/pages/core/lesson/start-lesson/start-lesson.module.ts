import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartLessonPage } from './start-lesson';

@NgModule({
  declarations: [
    StartLessonPage,
  ],
  imports: [
    IonicPageModule.forChild(StartLessonPage),
  ],
})
export class StartLessonPageModule {}
