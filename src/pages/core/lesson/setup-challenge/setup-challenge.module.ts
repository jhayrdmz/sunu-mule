import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetupChallengePage } from './setup-challenge';

@NgModule({
  declarations: [
    SetupChallengePage,
  ],
  imports: [
    IonicPageModule.forChild(SetupChallengePage),
  ],
})
export class SetupChallengePageModule {}
