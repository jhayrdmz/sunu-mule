import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoChallengesPage } from './no-challenges';

@NgModule({
  declarations: [
    NoChallengesPage,
  ],
  imports: [
    IonicPageModule.forChild(NoChallengesPage),
  ],
})
export class NoChallengesPageModule {}
