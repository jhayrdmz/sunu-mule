import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChallengeResultPage } from './challenge-result';

@NgModule({
  declarations: [
    ChallengeResultPage,
  ],
  imports: [
    IonicPageModule.forChild(ChallengeResultPage),
  ],
})
export class ChallengeResultPageModule {}
