import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChallengeDonePage } from './challenge-done';

@NgModule({
  declarations: [
    ChallengeDonePage,
  ],
  imports: [
    IonicPageModule.forChild(ChallengeDonePage),
  ],
})
export class ChallengeDonePageModule {}
